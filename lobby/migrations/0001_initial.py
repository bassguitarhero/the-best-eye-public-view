# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment_text', models.TextField(null=True, blank=True)),
                ('id_unique', models.IntegerField(default=0)),
                ('pubdate', models.DateTimeField(default=django.utils.timezone.now)),
                ('lastedit', models.DateTimeField(default=django.utils.timezone.now)),
                ('likes', models.IntegerField(default=0)),
                ('createdby', models.ForeignKey(related_name='comment_created_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['pubdate'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('body', models.TextField(null=True, blank=True)),
                ('pubdate', models.DateTimeField(default=django.utils.timezone.now)),
                ('lastedit', models.DateTimeField(default=django.utils.timezone.now)),
                ('likes', models.IntegerField(default=0)),
                ('createdby', models.ForeignKey(related_name='post_created_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-pubdate'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='comment',
            name='reply_to',
            field=models.ForeignKey(related_name='post_replied_to', to='lobby.Post'),
            preserve_default=True,
        ),
    ]
