from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'lobby.views.home'),
    url(r'^all/$', 'lobby.views.home'),
    url(r'^new/$', 'lobby.views.new_post'),
    url(r'^posts/new/$', 'lobby.views.new_post'),
    url(r'^posts/(?P<post_id>\d+)/$', 'lobby.views.post'),
    url(r'^posts/(?P<post_id>\d+)/edit/$', 'lobby.views.edit_post'),
    url(r'^posts/(?P<post_id>\d+)/delete/$', 'lobby.views.delete_post'),
    url(r'^posts/(?P<post_id>\d+)/like/$', 'lobby.views.like_post'),
    url(r'^posts/(?P<post_id>\d+)/unlike/$', 'lobby.views.unlike_post'),
    url(r'^posts/(?P<post_id>\d+)/new_comment/$', 'lobby.views.new_comment'),
    url(r'^posts/(?P<post_id>\d+)/comments/(?P<comment_id>\d+)/like/$', 'lobby.views.like_comment'),
    url(r'^posts/(?P<post_id>\d+)/comments/(?P<comment_id>\d+)/unlike/$', 'lobby.views.unlike_comment'),
    url(r'^posts/(?P<post_id>\d+)/comments/(?P<comment_id>\d+)/edit/$', 'lobby.views.edit_comment'),
    url(r'^posts/(?P<post_id>\d+)/comments/(?P<comment_id>\d+)/delete/$', 'lobby.views.delete_comment'),
    )
