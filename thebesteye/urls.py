from django.conf.urls import patterns, include, url
from django.contrib import admin
from thebesteye.forms import ContactForm1, ContactForm2, ContactForm3
from thebesteye.views import ContactWizard

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'thebesteye.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'thebesteye.views.home'),
    (r'^games/', include('game.urls')),
    (r'^accounts/', include('allauth.urls')),
    (r'^accounts/', include('userprofile.urls')),
    (r'^lobby/', include('lobby.urls')),
    (r'^eyechat/', include('eyechat.urls')),
    (r'^sitemessages/', include('notification.urls')),
    (r'^blog/', include('blog.urls')),
    (r'^activity/', include('activity.urls')),
    (r'^favorites/', include('favorite.urls')),
    (r'^leaderboards/', include('leaderboard.urls')),
    url(r'^contact/$', ContactWizard.as_view([ContactForm1, ContactForm2, ContactForm3])),
#    url(r'^resetpassword/passwordsent/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
#    url(r'^resetpassword/$', 'django.contrib.auth.views.password_reset', name='password_reset'),
#    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
#    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),

    # admin
    url(r'^admin/', include(admin.site.urls)),

    # user auth urls
#    url(r'^accounts/login/$', 'thebesteye.views.login'),
#    url(r'^accounts/auth/$', 'thebesteye.views.auth_view'),
#    url(r'^accounts/logout/$', 'thebesteye.views.logout'),
#    url(r'^accounts/loggedin/$', 'thebesteye.views.loggedin'),
#    url(r'^accounts/invalid/$', 'thebesteye.views.invalid'),
#    url(r'^accounts/register/$', 'thebesteye.views.register_user'),
#    url(r'^accounts/register_success/$', 'thebesteye.views.register_success'),
)
