from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.conf import settings
from django.template import RequestContext
from django.core.mail import send_mail
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import Library
from forms import NewUserRegistrationForm
from userprofile.models import UserProfile
from notification.models import SiteMessage
from game.models import Game, Result
from formtools.wizard.views import SessionWizardView
import logging
from favorite.models import FavoriteGame
logr = logging.getLogger(__name__)

register = Library()

def home(request):
	if not request.user.is_authenticated():
		thisuser = None
		s = None
		followers = None
		following = 0
		games_list = Game.objects.filter(private_game=False).order_by('-pubdate')
		new_game_available = False
		paginator = Paginator(games_list, 20)
		next = request.path

		page = request.GET.get('page')
		try:
			games = paginator.page(page)
		except PageNotAnInteger:
			games = paginator.page(1)
		except EmptyPage:
			games = paginator.page(paginator.num_pages)

		if request.method == "POST":
			search_text = request.POST['search_all_games']
			search_game_results = Game.objects.filter(Q(name__contains=search_text) | Q(description__contains=search_text), Q(createdby=thisuser) | Q(private=thisuser) | Q(private_game=False)).order_by('-pubdate')
			for game in search_game_results:
				if Result.objects.filter(game=game).exists():
					game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
			search_sent = True
		else: 
			search_game_results = None
			search_sent = False

		for game in games:
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]

	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()
		following = thisuser.user_object.following.count()
		games_list = Game.objects.filter(Q(private_game=False) | Q(private=thisuser) | Q(createdby=thisuser)).order_by('-pubdate')
		new_game_available = True
		paginator = Paginator(games_list, 20)
		next = request.path

		page = request.GET.get('page')
		try:
			games = paginator.page(page)
		except PageNotAnInteger:
			games = paginator.page(1)
		except EmptyPage:
			games = paginator.page(paginator.num_pages)

		for game in games:
			game.played = Result.objects.filter(game=game, player=thisuser).exists()
			game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
			game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
			if Result.objects.filter(game=game).exists():
				game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]

		if request.method == "POST":
			search_text = request.POST['search_all_games']
			search_game_results = Game.objects.filter(Q(name__contains=search_text) | Q(description__contains=search_text), Q(createdby=thisuser) | Q(private=thisuser) | Q(private_game=False)).order_by('-pubdate')
			for game in search_game_results:
				game.played = Result.objects.filter(game=game, player=thisuser).exists()
				game.liked = Game.objects.filter(id=game.id, gamelikes=thisuser).exists()
				game.favorite = thisuser.user_object.favorite_games.filter(id=game.id).exists()
				if Result.objects.filter(game=game).exists():
					game.latest = Result.objects.filter(game=game).order_by('-pubdate')[0]
			search_sent = True
		else: 
			search_game_results = None
			search_sent = False

	args = {'thisuser': thisuser, 'games': games, 'following': following, 'followers': followers, 's': s, 'search_game_results': search_game_results, 'search_sent': search_sent, 'new_game_available': new_game_available}
	args.update(csrf(request))

	return render(request, 'games.html', args)

class ContactWizard(SessionWizardView):
	template_name = "contact_form.html"
	@login_required
	def done(self, request, form_list, **kwargs):
		thisuser = User.objects.get(username=self.request.user.username)
		s = SiteMessage.objects.filter(user__username=self.request.user.username, viewed=False).order_by('-pubdate')
		followers = UserProfile.objects.filter(following=thisuser).count()

		form_data = process_form_data(form_list)

		args = {'form_data': form_data, 'thisuser': thisuser, 's': s, 'followers': followers}

		return render_to_response('done.html', args)

def process_form_data(form_list):
	form_data = [form.cleaned_data for form in form_list]

	logr.debug(form_data[0]['subject'])
	logr.debug(form_data[1]['sender'])
	logr.debug(form_data[2]['message'])

	send_mail(form_data[0]['subject'],
			form_data[2]['message'], form_data[1]['sender'],
			['jb@jasonboyce.com'], fail_silently=False)

	return form_data

def handler404(request):
	response = render_to_response('404.html', {},
		context_instance=RequestContext(request))
	response.status_code = 404
	return response


def handler500(request):
	response = render_to_response('500.html', {},
		context_instance=RequestContext(request))
	response.status_code = 500
	return response

def handler403(request):
	response = render_to_response('403.html', {},
		context_instance=RequestContext(request))
	response.status_code = 403
	return response