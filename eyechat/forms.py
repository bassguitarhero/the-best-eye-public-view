from django import forms
from models import Message
from django.contrib.auth.models import User

class MessageForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ('subject', 'body',)
        widgets = {
        	'subject': forms.TextInput(attrs={'size': '45'}),
        	'body': forms.Textarea(attrs={'rows': '3'}),
        }