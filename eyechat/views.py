from django.shortcuts import render_to_response, redirect, render
from django.shortcuts import get_object_or_404
from models import Message
from forms import MessageForm
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from datetime import datetime
from userprofile.models import UserProfile
from django.contrib import messages
from django.conf import settings
import csv
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from notification.models import SiteMessage

register = Library()

# Create your views here.

@login_required
def messages_all(request):
	if not request.user.is_authenticated():
		messages_all = None
		thisuser = None
		s = None
	else:
		thisuser = request.user
		s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
		if Message.objects.filter(sender=thisuser, sender_deleted=False).exists():
			any_messages = True
		else:
			if Message.objects.filter(recipient=thisuser, recipient_deleted=False).exists():
				any_messages = True
			else:
				any_messages = False
		if Message.objects.filter(sender=thisuser, sender_deleted=False).exists():
			available_sent_messages = Message.objects.filter(sender=thisuser, sender_deleted=False).order_by('-pubdate')
		else:
			available_sent_messages = None
		if Message.objects.filter(recipient=thisuser, recipient_deleted=False).exists():
			available_received_messages = Message.objects.filter(recipient=thisuser, recipient_deleted=False).order_by('-pubdate')
		else:
			available_received_messages = None
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()


	args = {'thisuser': thisuser, 'available_sent_messages': available_sent_messages, 'available_received_messages': available_received_messages, 'any_messages': any_messages, 'following': following, 'followers': followers, 's': s}

	return render(request, "messages.html", args)

@login_required
def get_message(request, message_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if Message.objects.filter(Q(recipient=thisuser) | Q(sender=thisuser), id=message_id).exists():
		message_view = Message.objects.get(id=message_id)
		if Message.objects.filter(id=message_id, recipient=thisuser).exists():
			if message_view.read_date:
				pass
			else:
				message_view.read_date = timezone.now()
				message_view.save()
		else:
			pass
	else:
		message_view = None

	args = {'thisuser': thisuser, 'message_view': message_view, 'following': following, 'followers': followers, 's': s}

	return render(request, "message.html", args)

@login_required
def new_message(request, message_id=1, user_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if User.objects.filter(id=user_id).exists():
		send_to = User.objects.get(id=user_id)
		if UserProfile.objects.filter(user=send_to, following=thisuser).exists():
			if request.POST:
				form = MessageForm(request.POST)
				if form.is_valid():
					f = form.save(commit=False)
					f.sender = request.user
					f.recipient = send_to
					f.save()

					messages.add_message(request, messages.SUCCESS, "Your Message was sent.")
					latest_message = Message.objects.filter(sender=thisuser).order_by('-pubdate')[0]

					new_subject = 'New Message!'
					new_message = '%s sent you a message! <a href="/eyechat/%s/">Check it out</a>' % (f.sender.username, latest_message.id)
					new_notification = SiteMessage.objects.create(subject=new_subject, message=new_message, user=f.recipient)

					return HttpResponseRedirect('/eyechat/')
			else:
				form = MessageForm()

				args = {'thisuser': thisuser, 'send_to': send_to, 'user_id': user_id, 'following': following, 'followers': followers, 's': s}
				args.update(csrf(request))

				args['form'] = form

				return render(request, 'new_message.html', args)

		return HttpResponseRedirect('/accounts/profile/%s/' % user_id)
	
	else:
		return HttpResponseRedirect('/')

@login_required
def delete_message(request, message_id=1):
	thisuser = request.user
	if Message.objects.filter(sender=thisuser, id=message_id).exists():
		message_to_delete = Message.objects.get(id=message_id)
		message_to_delete.sender_deleted = True
		message_to_delete.save()
		messages.add_message(request,
							settings.DELETE_MESSAGE,
							"Your Message was deleted.")
		return HttpResponseRedirect('/eyechat/')
	else:
		if Message.objects.filter(recipient=thisuser, id=message_id).exists():
			message_to_delete = Message.objects.get(id=message_id)
			message_to_delete.recipient_deleted = True
			message_to_delete.save()
			messages.add_message(request,
							settings.DELETE_MESSAGE,
							"Your Message was deleted.")
			return HttpResponseRedirect('/eyechat/')
		else:
			messages.add_message(request,
							settings.DELETE_MESSAGE,
							"No Message to delete.")
			return HttpResponseRedirect('/eyechat/')

@login_required
def reply_message(request, message_id=1):
	thisuser = request.user
	if UserProfile.objects.filter(user=thisuser).exists():
		following = thisuser.user_object.following.count()
	else:
		following = 0
	followers = UserProfile.objects.filter(following=thisuser).count()
	s = SiteMessage.objects.filter(user=request.user, viewed=False).order_by('-pubdate')
	if Message.objects.filter(Q(sender=thisuser) | Q(recipient=thisuser), id=message_id).exists():
		message_view = Message.objects.get(id=message_id)
	else:
		message_view = None
	if request.method == "POST":
		form = MessageForm(request.POST)
		if form.is_valid():
			f = form.save(commit=False)
			f.sender = thisuser
			f.recipient = message_view.sender
			f.save()

			messages.add_message(request, messages.SUCCESS, "Your Reply was sent.")

			return HttpResponseRedirect('/eyechat/')

	else:
		form = MessageForm()

		args = {'thisuser': thisuser, 'message_view': message_view, 'following': following, 'followers': followers, 's': s}
		args.update(csrf(request))

		args['form'] = form

		return render(request, 'reply.html', args)