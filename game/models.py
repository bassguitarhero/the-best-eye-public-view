from django.db import models
from django.utils import timezone
from time import time
from django.contrib.auth.models import User
from PIL import Image
from PIL import _imaging
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose
from django.conf import settings
from django.core.files.storage import default_storage as storage

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

# Create your models here.

class Game(models.Model):
	name 			= models.CharField(max_length=200, null=True, blank=True)
	description 	= models.TextField(null=True, blank=True)
	pubdate 		= models.DateTimeField(default=timezone.now)
	thumbnail 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	game_thumbnail 	= ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(200,200),
			],
		format = 'JPEG', 
		options={'quality': 60},
		null = True,
		blank = True
		)
	createdby 		 = models.ForeignKey(User, related_name='game_creator')
	lastedit 		 = models.DateTimeField(default=timezone.now)
	private_game 	 = models.BooleanField(default=False)
	private 		 = models.ManyToManyField(User, related_name='private_game_players')
	item01 			 = models.CharField(max_length=200, null=True, blank=True)
	item01photocount = models.IntegerField(default=0)
	item02 			 = models.CharField(max_length=200, null=True, blank=True)
	item02photocount = models.IntegerField(default=0)
	item03 			 = models.CharField(max_length=200, null=True, blank=True)
	item03photocount = models.IntegerField(default=0)
	item04 			 = models.CharField(max_length=200, null=True, blank=True)
	item04photocount = models.IntegerField(default=0)
	item05 			 = models.CharField(max_length=200, null=True, blank=True)
	item06 			 = models.CharField(max_length=200, null=True, blank=True)
	item07 			 = models.CharField(max_length=200, null=True, blank=True)
	item08 			 = models.CharField(max_length=200, null=True, blank=True)
	item09 			 = models.CharField(max_length=200, null=True, blank=True)
	item10 			 = models.CharField(max_length=200, null=True, blank=True)
	value			 = models.CharField(max_length=200, null=True, blank=True)
	endby_date		 = models.DateField(null=True, blank=True)
	game_closed		 = models.BooleanField(default=False)
	location		 = models.CharField(max_length=200, null=True, blank=True)
	gamelikes 		 = models.ManyToManyField(User, related_name='game_likes')
	photocount 		 = models.IntegerField(default=0)
	hide_latest		 = models.BooleanField(default=False)

	def __unicode__(self):
		return self.name

	def save(self):
		super(Game, self).save()
		if self.thumbnail:
			size = 200, 200
			image = Image.open(self.thumbnail)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.thumbnail.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
                
	def get_thumbnail(self):
		thumb = str(self.thumbnail)
		if not settings.DEBUG:
			thumb = thumb.replace('assets/', '')
			return thumb

		game_thumb = str(self.game_thumbnail)
		if not settings.DEBUG:
			game_thumb = game_thumb.replace('assets/', '')
			return game_thumb

class RandomGameItem(models.Model):
	item 	= models.CharField(max_length=200, null=True, blank=True)
	user 	= models.ForeignKey(User, related_name='suggestion_creator')
	pubdate	= models.DateTimeField(default=timezone.now)

	def __unicode__(self):
		return self.item

class GameComment(models.Model):
	game = models.ForeignKey(Game, related_name='game_for_comment')
	usertocomment = models.ForeignKey(User, related_name='game_comment')
	unique_id = models.IntegerField(default=1)
	comment = models.CharField(max_length=200, null=True, blank=True)
	pubdate = models.DateTimeField(default=timezone.now)
	lastedit = models.DateTimeField(default=timezone.now)
	gamecommentlikes = models.ManyToManyField(User, related_name='game_comment_likes')

	def __unicode__(self):
		return self.game.name

class Result(models.Model):
	game 			= models.ForeignKey(Game, related_name='game_host')
	player 			= models.ForeignKey(User, related_name='game_player')
	pubdate 		= models.DateTimeField(default=timezone.now)
	lastedit 		= models.DateTimeField(default=timezone.now)
	disqualified	= models.BooleanField(default=False)
	resultlikes 	= models.ManyToManyField(User, related_name='result_likes')
	location 		= models.CharField(max_length=200, null=True, blank=True)
	result01 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result01thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result01crop	= ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(1000,1000),
			],
		format = 'JPEG', 
		options={'quality': 90},
		null = True,
		blank = True
		)
	result01desc	= models.CharField(max_length=100, null=True, blank=True)
	result01likes 	= models.ManyToManyField(User, related_name='result_01_likes')
	result02 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result02thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result02crop	= ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(1000,1000),
			],
		format = 'JPEG', 
		options={'quality': 90},
		null = True,
		blank = True
		)
	result02desc	= models.CharField(max_length=100, null=True, blank=True)
	result02likes 	= models.ManyToManyField(User, related_name='result_02_likes')
	result03 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result03thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result03crop	= ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(1000,1000),
			],
		format = 'JPEG', 
		options={'quality': 90},
		null = True,
		blank = True
		)
	result03desc	= models.CharField(max_length=100, null=True, blank=True)
	result03likes 	= models.ManyToManyField(User, related_name='result_03_likes')
	result04 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result04thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result04crop	= ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(1000,1000),
			],
		format = 'JPEG', 
		options={'quality': 90},
		null = True,
		blank = True
		)
	result04desc	= models.CharField(max_length=100, null=True, blank=True)
	result04likes 	= models.ManyToManyField(User, related_name='result_04_likes')
	result05 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result05thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result05likes 	= models.ManyToManyField(User, related_name='result_05_likes')
	result06 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result06thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result06likes 	= models.ManyToManyField(User, related_name='result_06_likes')
	result07 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result07thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result07likes 	= models.ManyToManyField(User, related_name='result_07_likes')
	result08 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result08thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result08likes 	= models.ManyToManyField(User, related_name='result_08_likes')
	result09 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result09thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result09likes 	= models.ManyToManyField(User, related_name='result_09_likes')
	result10 		= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result10thumb	= models.FileField(upload_to=get_upload_file_name, null=True, blank=True)
	result10likes 	= models.ManyToManyField(User, related_name='result_10_likes')
	result_points	= models.IntegerField(default=1)
	gamewinner		= models.BooleanField(default=False)

	def __unicode__(self):
		return self.player.username

	def save(self):
		super(Result, self).save()
		if self.result01:
			size = 1667, 1250
			image = Image.open(self.result01)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result01.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result02:
			size = 1667, 1250
			image = Image.open(self.result02)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result02.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result03:
			size = 1667, 1250
			image = Image.open(self.result03)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result03.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result04:
			size = 1667, 1250
			image = Image.open(self.result04)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result04.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result05:
			size = 1667, 1250
			image = Image.open(self.result05)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result05.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result06:
			size = 1667, 1250
			image = Image.open(self.result06)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result06.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result07:
			size = 1667, 1250
			image = Image.open(self.result07)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result07.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result08:
			size = 1667, 1250
			image = Image.open(self.result08)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result08.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result09:
			size = 1667, 1250
			image = Image.open(self.result09)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result09.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
		if self.result10:
			size = 1667, 1250
			image = Image.open(self.result10)
			image.thumbnail(size, Image.ANTIALIAS)
			fh = storage.open(self.result10.name, "w")
			format = 'png'  # You need to set the correct image format here
			image.save(fh, format)
			fh.close()
                
class ResultComment(models.Model):
	result = models.ForeignKey(Result, related_name='result_for_comment')
	usertocomment = models.ForeignKey(User, related_name='result_comment')
	unique_id = models.IntegerField(default=1)
	comment = models.CharField(max_length=200, null=True, blank=True)
	pubdate = models.DateTimeField(default=timezone.now)
	lastedit = models.DateTimeField(default=timezone.now)
	resultcommentlikes = models.ManyToManyField(User, related_name='result_comment_likes')
		
class ResultItemComment(models.Model):
	result = models.ForeignKey(Result, related_name='result_for_item_comment')
	itemno = models.IntegerField(default=1)
	usertocomment = models.ForeignKey(User, related_name='user_to_comment_on_result_item')
	unique_id = models.IntegerField(default=1)
	comment = models.CharField(max_length=200, null=True, blank=True)
	pubdate = models.DateTimeField(default=timezone.now)
	lastedit = models.DateTimeField(default=timezone.now)
	resultitemcommentlikes = models.ManyToManyField(User, related_name='users_who_like_item_comment')

class Achievement(models.Model):
	name 		= models.CharField(max_length=200, null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	value 		= models.CharField(max_length=200, null=True, blank=True)
	trophy 		= ProcessedImageField(
		upload_to=get_upload_file_name,
		processors=[
			Transpose(),
			SmartResize(300,300),
			],
		format = 'JPEG', 
		options={'quality': 60},
		null = True,
		blank = True
		)
	createdby 	= models.ForeignKey(User, related_name='achievement_creator')
	pubdate 	= models.DateTimeField(default=timezone.now)
	lastedit 	= models.DateTimeField(default=timezone.now)

	def __unicode__(self):
		return self.name