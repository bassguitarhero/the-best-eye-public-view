from django import forms
from models import Game, Result, GameComment, ResultComment, ResultItemComment

class GameForm(forms.ModelForm):

	class Meta:
		model = Game
		fields = ('name', 'description', 'game_thumbnail', 'thumbnail', 'private_game', 'item01', 'item02', 'item03', 'item04', 'item05', 'item06', 'item07', 'item08', 'item09', 'item10', 'endby_date', 'hide_latest')
		widgets = {
			'name': forms.TextInput(attrs={'size': '45'}),
			'item01': forms.TextInput(attrs={'size': '60'}),
			'item02': forms.TextInput(attrs={'size': '60'}),
			'item03': forms.TextInput(attrs={'size': '60'}),
			'item04': forms.TextInput(attrs={'size': '60'}),
		}

class GameCommentForm(forms.ModelForm):

	class Meta:
		model = GameComment
		fields = ('comment',)

class ResultForm(forms.ModelForm):

	class Meta:
		model = Result
		fields = ('result01crop', 'result01desc', 'result02crop', 'result02desc', 'result03crop', 'result03desc', 'result04crop', 'result04desc')

class ResultCommentForm(forms.ModelForm):

	class Meta:
		model = ResultComment
		fields = ('comment',)

class ResultItemCommentForm(forms.ModelForm):

	class Meta:
		model = ResultItemComment
		fields = ('comment',)